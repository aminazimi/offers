import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JobOfferModule } from './offer/offer.module';

@Module({
  imports: [JobOfferModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
