import { Injectable } from '@nestjs/common';
import { ProvidersEnum } from './enums';
import { PayloadMapping } from './payload-mapping';
import { IOffer } from './interfaces/IOffer';

@Injectable()
export class OfferService {
  public async saveOffers(
    providerName: ProvidersEnum,
    payload: any,
  ): Promise<void> {
    try {
      const offers: IOffer[] = await new PayloadMapping(providerName).process(
        payload,
      );
      console.log(
        'precessed offers that can be saved inDB........................',
        offers,
      );
      //this.offerRepository.save(offers);
    } catch (error) {
      throw new Error(`Failed to save offers: ${error.message}`);
    }
  }
}
