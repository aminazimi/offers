import { plainToInstance } from 'class-transformer';
import { ProvidersEnum } from '../enums';
import { IOffer } from '../interfaces/IOffer';
import { validate } from 'class-validator';
import { ProviderOneOfferDTO } from '../dtos/provider-one.dto';
import { IPayloadTransformer } from '../interfaces';
import { ProviderTwoOfferDTO } from '../dtos/provider-two.dto';

export abstract class PayloadTransformerBase<T>
  implements IPayloadTransformer<T>
{
  constructor(protected readonly providerName: ProvidersEnum) {}

  public abstract transform(payload: T): Promise<IOffer[]>;

  async validatePayload(offerPayload: any): Promise<boolean> {
    const dto = this.createDTO(offerPayload);
    const errors = await validate(dto as object);
    console.warn(errors);
    return errors.length === 0;
  }

  private createDTO(offerPayload: any): any {
    switch (this.providerName) {
      case ProvidersEnum.PROVIDER_ONE:
        const providerOneInstance = plainToInstance(
          ProviderOneOfferDTO,
          offerPayload,
        );
        return providerOneInstance;
      case ProvidersEnum.PROVIDER_TWO:
        const providerTwoInstance = plainToInstance(
          ProviderTwoOfferDTO,
          offerPayload,
        );
        return providerTwoInstance;
      default:
        throw new Error(`DTO not found for provider: ${this.providerName}`);
    }
  }
}
