import { ProviderOneOfferDTO, ProviderOneDTO } from '../dtos/provider-one.dto';
import { Offer } from '../entities/offer.entity';
import { ProvidersEnum } from '../enums';
import { IOffer } from '../interfaces/IOffer';
import { PayloadTransformerBase } from './payload-base-transformer';

export class ProviderOneTransformer extends PayloadTransformerBase<ProviderOneDTO> {
  constructor() {
    super(ProvidersEnum.PROVIDER_ONE);
    this.transform = this.transform.bind(this);
  }
  async transform(payload: ProviderOneDTO): Promise<IOffer[]> {
    const offers: ProviderOneOfferDTO[] = payload.response.offers;
    const mappedOffers: IOffer[] = [];

    for (const offerDTO of offers) {
      if (await this.validatePayload(offerDTO)) {
        const offerEntity = new Offer();
        offerEntity.providerName = this.providerName;
        offerEntity.name = offerDTO.offer_name;
        offerEntity.description = offerDTO.offer_desc;
        offerEntity.requirements = offerDTO.call_to_action;
        offerEntity.thumbnail = offerDTO.image_url;
        offerEntity.offerUrlTemplate = offerDTO.offer_url;
        offerEntity.isDesktop = offerDTO.platform === 'desktop' ? 1 : 0;
        offerEntity.isAndroid = offerDTO.device === 'android' ? 1 : 0;
        offerEntity.isIos = offerDTO.device === 'iphone_ipad' ? 1 : 0;
        offerEntity.externalOfferId = offerDTO.offer_id;

        mappedOffers.push(offerEntity);
      }
    }

    return mappedOffers;
  }
}
