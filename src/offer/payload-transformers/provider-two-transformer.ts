import { ProviderTwoDTO } from '../dtos/provider-two.dto';
import { IOffer } from '../interfaces';
import { PayloadTransformerBase } from './payload-base-transformer';
import { Offer } from '../entities/offer.entity';
import { ProvidersEnum } from '../enums';

export class ProviderTwoTransformer extends PayloadTransformerBase<ProviderTwoDTO> {
  constructor() {
    super(ProvidersEnum.PROVIDER_TWO);
    this.transform = this.transform.bind(this);
  }

  public async transform(payload: ProviderTwoDTO): Promise<IOffer[]> {
    const campaignId = Object.keys(payload.data)[0];
    const offer = !!campaignId ? payload.data[campaignId].Offer : undefined;
    const osData = !!campaignId ? payload.data[campaignId].OS : undefined;
    if (offer && (await this.validatePayload(offer))) {
      const offerEntity = new Offer();
      offerEntity.providerName = this.providerName;
      offerEntity.name = offer.name;
      offerEntity.description = offer.description;
      offerEntity.externalOfferId = offer.campaign_id.toString();
      offerEntity.thumbnail = offer.icon;
      offerEntity.offerUrlTemplate = offer.tracking_url;
      offerEntity.requirements = offer.instructions;
      offerEntity.isAndroid = osData.android ? 1 : 0;
      offerEntity.isIos = osData.ios ? 1 : 0;
      offerEntity.isDesktop = osData.web ? 1 : 0;
      return [offerEntity];
    }
    return [];
  }
}
