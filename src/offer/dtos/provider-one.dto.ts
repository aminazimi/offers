import {
  IsString,
  IsUrl,
  IsArray,
  ValidateNested,
  IsEnum,
} from 'class-validator';
import { Type } from 'class-transformer';

enum Platform {
  desktop = 'desktop',
  mobile = 'mobile',
}

export class ProviderOneOfferDTO {
  @IsString()
  offer_id: string;

  @IsString()
  offer_name: string;

  @IsString()
  offer_desc: string;

  @IsString()
  call_to_action: string;

  @IsUrl()
  offer_url: string;

  @IsUrl()
  image_url: string;

  @IsEnum(Platform)
  @IsString()
  platform: string;

  @IsString()
  device: string;
}

export class ResponseDTO {
  currency_name: string;
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ProviderOneOfferDTO)
  offers: ProviderOneOfferDTO[];
}

export class ProviderOneDTO {
  @ValidateNested()
  @Type(() => ResponseDTO)
  response: ResponseDTO;
}
