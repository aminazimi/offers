import {
  IsString,
  IsUrl,
  IsNumber,
  ValidateNested,
  IsBoolean,
} from 'class-validator';
import { Type } from 'class-transformer';

export class OsDTO {
  // this should be mapped to `isAndroid`
  @IsBoolean()
  android: boolean;

  // this should be mapped to `isIos`
  @IsBoolean()
  ios: boolean;

  // this should be mapped to `isDesktop`
  @IsBoolean()
  web: boolean;
}

export class ProviderTwoOfferDTO {
  // should be mapped to `externalOfferId`
  @IsNumber()
  campaign_id: number;

  // should be mapped to `thumbnail`
  @IsUrl()
  icon: string;

  // should be mapped to `name`
  @IsString()
  name: string;

  // should be mapped to `offerUrlTemplate`
  @IsUrl()
  tracking_url: string;

  // should be mapped to `requirements`
  @IsString()
  instructions: string;

  // should be mapped to `description`
  @IsString()
  description: string;
}

class DataDTO {
  @ValidateNested()
  @Type(() => ProviderTwoOfferDTO)
  Offer: ProviderTwoOfferDTO;

  @ValidateNested()
  @Type(() => OsDTO)
  OS: OsDTO;
}

export class ProviderTwoDTO {
  @ValidateNested()
  @Type(() => DataDTO)
  data: { [key: string]: DataDTO };
}
