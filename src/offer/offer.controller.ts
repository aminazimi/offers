import { Controller, Get } from '@nestjs/common';
import { OfferService } from './offer.service';
import { ProvidersEnum } from './enums';
import { payloadOne, payloadTwo } from './dtos/sample';

@Controller('offer')
export class OfferController {
  constructor(private readonly offerService: OfferService) {}
  @Get()
  ping(): string {
    const payloads = [
      { providerName: ProvidersEnum.PROVIDER_ONE, payload: payloadOne },
      { providerName: ProvidersEnum.PROVIDER_TWO, payload: payloadTwo },
    ];

    for (const payload of payloads) {
      this.offerService.saveOffers(payload.providerName, payload.payload);
    }

    return 'pong';
  }
}
