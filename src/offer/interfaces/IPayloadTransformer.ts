import { IOffer } from './IOffer';

export interface IPayloadTransformer<T> {
  transform(payload: T): Promise<IOffer[]>;
}
