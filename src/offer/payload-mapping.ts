import { IOffer } from './interfaces/IOffer';
import { ProvidersEnum } from './enums/providers.enum';
import { ProviderOneTransformer } from './payload-transformers/provider-one-transformer';
import { ProviderTwoTransformer } from './payload-transformers/provider-two-transformer';

export class PayloadMapping {
  private providerName: ProvidersEnum;
  private providers = [
    {
      provider: ProvidersEnum.PROVIDER_ONE,
      transform: new ProviderOneTransformer().transform,
    },
    {
      provider: ProvidersEnum.PROVIDER_TWO,
      transform: new ProviderTwoTransformer().transform,
    },
  ];

  constructor(providerName: ProvidersEnum) {
    this.providerName = providerName;
  }

  public async process(payload: any): Promise<IOffer[]> {
    const result = this.providers.find(
      (provider) => provider.provider === this.providerName,
    );
    if (!result) {
      throw new Error(`Mapping not found for provider: ${this.providerName}`);
    }

    return result.transform(payload);
  }
}
